# Food Delivery App

## Setup Guide

This section focuses on the Step-by-step instruction to clone the project locally and install the needed Flutter packages.

1. Open GitBash or Terminal in your computer.
2. Change the current working directory to the location where you want the cloned directory.
3. Type `git clone`, and then paste the URL you copied earlier. `git clone <project_url>`.
4. Navigate to the flutter project by using the command `cd food_delivery_application`.
5. Type `flutter pub get` to install the required packages in `pubspec.yaml` to your device.
6. Type `flutter run` to start building or running the cloned flutter application. (Note: Dart and Flutter SDK must be already installed in your computer, otherwise follow the instructions in the provided [link](https://flutter.dev/docs/get-started/install/windows)).

# Installed Packages

The following are the packages installed in pubspec.yaml with their respective description: 

- **[font_awesome_flutter](https://pub.dev/packages/font_awesome_flutter) 9.1.0**- The Font Awesome Icon pack available as Flutter Icons. Provides 1500 additional icons to use in your apps
- **[google_fonts](https://pub.dev/packages/google_fonts) 2.1.0**- a package to include fonts from [fonts.google.com](http://fonts.google.com/) in your Flutter app.
- **[provider](https://pub.dev/packages/provider) 5.0.0**- is a wrapper around InheritedWidget to make them easier to use and more reusable.
- **sqflite**: **^2.0.0+4**: Flutter plugin for SQLite, a self-contained, high-reliability, embedded, SQL database engine.
- **animated_splash_screen: ^1.1.0** - The easiest way to create your animated splash screen in a fully customizable way.
- **badges: ^2.0.1** - A flutter package for creating badges. Badges can be used for an additional marker for any widget, e.g. show a number of items in a shopping cart.

## Flutter Version

The flutter version can be seen from pubspec.yaml which was set to ">=2.12.0 <3.0.0". 

The most important note in the version used was the introduction of Flutter null safety which is a feature that was made accessible following the release of version 2 of the Dart programming language, which has a minimum version of 2.12.

# Screens

![Screenshot_1631868911.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868911.png)

![Screenshot_1631868916.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868916.png)

![Screenshot_1631868923.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868923.png)

![Screenshot_1631868927.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868927.png)

![Screenshot_1631868935.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868935.png)

![Screenshot_1631868938.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868938.png)

![Screenshot_1631868952.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868952.png)

![Screenshot_1631868955.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868955.png)

![Screenshot_1631868958.png](Food%20Delivery%20App%204cc1a5abcbaf49f6992606fc35a66c88/Screenshot_1631868958.png)

# SQfLite

```dart
//MENU CATEGORY TABLE
static MenuCategory fromJson(Map<String, Object?> json) => MenuCategory(
        id: json[MenuCategoryFields.id] as int?,
        title: json[MenuCategoryFields.title] as String,
        image: json[MenuCategoryFields.image] as String,
      );

//MENU PRODUCT TABLE
static MenuProduct fromJson(Map<String, Object?> json) => MenuProduct(
        id: json[MenuProductFields.id] as int?,
        title: json[MenuProductFields.title] as String,
        image: json[MenuProductFields.image] as String,
        price: json[MenuProductFields.price] as num,
        categoryId: json[MenuProductFields.categoryId] as int,
      );
//ORDER DETAILS
static OrderDetails fromJson(Map<String, Object?> json) => OrderDetails(
        id: json[OrderDetailsFields.id] as int?,
        menuProductId: json[OrderDetailsFields.menuProductId] as int,
        amount: json[OrderDetailsFields.amount] as num,
        noOfServing: json[OrderDetailsFields.noOfServing] as int,
        totalAmount: json[OrderDetailsFields.totalAmount] as num,
      );

Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    final integerType = 'INTEGER NOT NULL';
    final realType = 'REAL NOT NULL';

    await db.execute('''
CREATE TABLE $tableMenuCategory ( 
  ${MenuCategoryFields.id} $idType, 
  ${MenuCategoryFields.title} $textType,
  ${MenuCategoryFields.image} $textType
  )
''');

    await db.execute('''
CREATE TABLE $tableMenuProduct ( 
  ${MenuProductFields.id} $idType, 
  ${MenuProductFields.title} $textType,
  ${MenuProductFields.image} $textType,
  ${MenuProductFields.price} $realType,
  ${MenuProductFields.categoryId} $integerType
  )
''');

    await db.execute('''
CREATE TABLE $tableOrderDetails ( 
  ${OrderDetailsFields.id} $idType, 
  ${OrderDetailsFields.menuProductId} $integerType,
  ${OrderDetailsFields.amount} $realType,
  ${OrderDetailsFields.noOfServing} $integerType,
  ${OrderDetailsFields.totalAmount} $realType
  )
''');
  }

```