import 'package:flutter/material.dart';
import '/models/menu_category.dart';

class MenuCategoryItem extends StatelessWidget {
  MenuCategoryItem({
    Key? key,
    required this.category,
    required this.index,
  }) : super(key: key);

  final MenuCategory category;
  final int index;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: GridTile(
        child: Image.asset(
          category.image,
          fit: BoxFit.cover,
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black45,
          title: Text(
            category.title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 16,
                color: Colors.grey[100],
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
