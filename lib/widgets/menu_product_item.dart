import 'package:flutter/material.dart';
import 'package:food_delivery_application/models/menu_product.dart';
import '/models/menu_product.dart';

class MenuProductItem extends StatelessWidget {
  MenuProductItem({
    Key? key,
    required this.product,
    required this.index,
  }) : super(key: key);

  final MenuProduct product;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: Container(
              color: Colors.grey[200],
              child: Image(
                image: AssetImage(product.image),
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Text(product.title,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
          ),
          Container(
            margin: EdgeInsets.only(top: 2),
            child: Text(
              "\$" + product.price.toString(),
              style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.grey[700]),
            ),
          )
        ],
      ),
    );
  }
}
