import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '/db/database.dart';
import '/models/menu_product.dart';
import '/models/order_details.dart';
import '/screens/food_cart_screen.dart';
import '/widgets/snackbar.dart';

class SingleProductItem extends StatefulWidget {
  SingleProductItem({
    Key? key,
    required this.product,
  }) : super(key: key);

  final MenuProduct product;

  @override
  _SingleProductItemState createState() => _SingleProductItemState();
}

class _SingleProductItemState extends State<SingleProductItem> {
  late List<OrderDetails> order;
  int qty = 1;
  bool isLoading = false;

  void initState() {
    super.initState();
    refreshOrder();
  }

  Future refreshOrder() async {
    setState(() => isLoading = true);

    this.order =
        await DatabaseHelper.instance.readOrderDetails(widget.product.id!);

    if (order.isEmpty) {
      this.order =
          await DatabaseHelper.instance.readOrderDetails(widget.product.id!);
    }

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: isLoading ? CircularProgressIndicator() : buildBody());
  }

  buildBody() {
    return SafeArea(
      child: Container(
          child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
              color: Colors.grey[200],
              child: Stack(
                children: <Widget>[
                  Center(
                    child: Image(
                      image: AssetImage(widget.product.image),
                      fit: BoxFit.fill,
                    ),
                  ),
                  Positioned(
                      top: 16,
                      left: 16,
                      child: SafeArea(
                        child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.arrow_back_ios_new,
                              color: Colors.black,
                            )),
                      )),
                  Positioned(
                      top: 16,
                      right: 16,
                      child: SafeArea(
                        child: InkWell(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          FoodCartScreen(isLeading: true)));
                            },
                            child: Badge(
                              badgeContent: Text(
                                Provider.of<OrderPrice>(context).counter,
                                style: TextStyle(color: Colors.white),
                              ),
                              child: FaIcon(FontAwesomeIcons.shoppingCart),
                            )),
                      ))
                ],
              )),
          Container(
            margin: EdgeInsets.only(top: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                InkWell(
                  onTap: () {
                    if (qty > 1)
                      setState(() {
                        qty--;
                      });
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Color(0xFF3865f8),
                      shape: BoxShape.circle,
                    ),
                    child: FaIcon(
                      FontAwesomeIcons.minus,
                      size: 14,
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Text(
                      qty.toString(),
                      style:
                          TextStyle(fontSize: 28, fontWeight: FontWeight.w700),
                    )),
                InkWell(
                  onTap: () {
                    setState(() {
                      qty++;
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                      color: Color(0xFF3865f8),
                      shape: BoxShape.circle,
                    ),
                    child: FaIcon(
                      FontAwesomeIcons.plus,
                      size: 14,
                      color: Colors.white,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 24),
              child: Text(
                widget.product.title,
                style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.w800,
                    color: Colors.grey[800]),
              )),
          Container(
              margin: EdgeInsets.only(left: 24, right: 24), child: Divider()),
          Container(
            margin: EdgeInsets.only(left: 24, right: 24, top: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Total \₱" + (widget.product.price * qty).toString(),
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w800,
                      color: Colors.grey[800]),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withAlpha(20),
                        blurRadius: 4,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: ElevatedButton(
                    onPressed: () {
                      addOrUpdateOrder();
                    },
                    child: Text(
                      "Add to Cart".toUpperCase(),
                      style: TextStyle(
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: Color(0xFF3865f8),
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 8)),
                  ),
                ),
              ],
            ),
          )
        ],
      )),
    );
  }

  void addOrUpdateOrder() async {
    if (order.length == 0) {
      await addProductToCart();
      print("~ add product to cart");
    } else {
      await updateProductInCart();
      print("~ updated product in cart");
    }
  }

  addProductToCart() async {
    await DatabaseHelper.instance.addOrderDetails(OrderDetails(
        menuProductId: widget.product.id!,
        amount: widget.product.price,
        noOfServing: qty,
        totalAmount: (widget.product.price * qty)));
    refreshOrder();
    showSnackBar(
        context, "Order Added: ${widget.product.title}", Colors.green, 3000);
  }

  updateProductInCart() async {
    final updatedOrder = order[0].copy(
        menuProductId: widget.product.id!,
        amount: widget.product.price,
        noOfServing: qty,
        totalAmount: (widget.product.price * qty));
    await DatabaseHelper.instance.updateOrderDetails(updatedOrder);
    refreshOrder();
  }
}
