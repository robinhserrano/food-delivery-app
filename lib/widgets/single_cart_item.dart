import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_delivery_application/db/database.dart';
import 'package:food_delivery_application/models/menu_product.dart';
import 'package:food_delivery_application/models/order_details.dart';
import 'package:food_delivery_application/screens/food_cart_screen.dart';
import 'package:food_delivery_application/widgets/snackbar.dart';
import 'package:provider/provider.dart';

class SingleCartItem extends StatefulWidget {
  SingleCartItem({
    Key? key,
    required this.order,
  }) : super(key: key);

  final OrderDetails order;

  @override
  _SingleCartItemState createState() => _SingleCartItemState();
}

class _SingleCartItemState extends State<SingleCartItem> {
  late ThemeData themeData;
  late int qty;
  late List<MenuProduct> product;
  bool isLoading = false;
  var subtotal;

  @override
  void initState() {
    getSubtotal();
    super.initState();
    refreshProducts();

    qty = widget.order.noOfServing;
  }

  Future refreshProducts() async {
    setState(() => isLoading = true);

    this.product = await DatabaseHelper.instance
        .readMenuProductById(widget.order.menuProductId);

    if (product.isEmpty) {
      this.product = await DatabaseHelper.instance
          .readMenuProductById(widget.order.menuProductId);
    }

    subtotal = await DatabaseHelper.instance.readOrderDetailsTotal();
    updateSubtotal();

    setState(() => isLoading = false);
  }

  Future getSubtotal() async {
    subtotal = await DatabaseHelper.instance.readOrderDetailsTotal();
  }

  @override
  Widget build(BuildContext context) {
    themeData = Theme.of(context);
    return Center(
      child: isLoading
          ? CircularProgressIndicator()
          : product.isEmpty
              ? Text(
                  'No Menu',
                  style: TextStyle(color: Colors.black, fontSize: 24),
                )
              : buildBody(),
    );
  }

  buildBody() {
    return qty > 0
        ? Container(
            margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  child: InkWell(
                    onTap: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => FoodProductScreen()));
                    },
                    child: Image(
                      image: AssetImage(product[0].image),
                      height: 100,
                      width: 100,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    height: 100,
                    margin: EdgeInsets.only(left: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          product[0].title,
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey[800],
                              fontWeight: FontWeight.w600),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 8),
                          child: Text(
                            "\$ " + (product[0].price * qty).toString(),
                            style: TextStyle(
                                fontSize: 17,
                                color: Colors.grey[800],
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  child: Column(
                    children: <Widget>[
                      Text(
                        qty.toString(),
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.w600),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 4),
                        child: Row(
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                setState(() {
                                  qty--;
                                });
                                updateProductInCart();
                                getSubtotal();
                                updateSubtotal();
                                if (qty <= 0) {
                                  removeOrder();
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color: Color(0xFF3865f8),
                                  shape: BoxShape.circle,
                                ),
                                child: FaIcon(
                                  FontAwesomeIcons.minus,
                                  size: 14,
                                  color: themeData.colorScheme.onPrimary,
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() {
                                  qty++;
                                });
                                updateProductInCart();
                                getSubtotal();
                                updateSubtotal();
                              },
                              child: Container(
                                padding: EdgeInsets.all(8),
                                margin: EdgeInsets.only(left: 8),
                                decoration: BoxDecoration(
                                  color: Color(0xFF3865f8),
                                  shape: BoxShape.circle,
                                ),
                                child: FaIcon(
                                  FontAwesomeIcons.plus,
                                  size: 14,
                                  color: themeData.colorScheme.onPrimary,
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        : Container();
  }

  removeOrder() async {
    var item = product[0].title;
    await DatabaseHelper.instance.removeOrderDetails(widget.order.id!);

    showSnackBar(context, "Order Removed: $item", Colors.red, 3000);
  }

  updateSubtotal() {
    Provider.of<OrderPrice>(context, listen: false)
        .updateSubtotal(subtotal.toString());
  }

  updateProductInCart() async {
    final updatedOrder = widget.order.copy(
        menuProductId: product[0].id!,
        amount: product[0].price,
        noOfServing: qty,
        totalAmount: (product[0].price * qty));
    await DatabaseHelper.instance.updateOrderDetails(updatedOrder);

    showSnackBar(
        context, "Order Updated: ${product[0].title}", Colors.green, 3000);
  }
}
