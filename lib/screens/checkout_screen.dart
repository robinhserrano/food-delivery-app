import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:food_delivery_application/db/database.dart';
import 'package:food_delivery_application/screens/completed_order_screen.dart';
import '/screens/food_cart_screen.dart';
import 'package:provider/provider.dart';

class FoodCheckoutScreen extends StatefulWidget {
  @override
  _FoodCheckoutScreenState createState() => _FoodCheckoutScreenState();
}

class _FoodCheckoutScreenState extends State<FoodCheckoutScreen> {
  int selectedAddress = 0;
  int? paymentMethod = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child:
                Icon(Icons.arrow_back_ios_new, size: 24, color: Colors.black),
          ),
          title: Text(
            "Checkout",
            style: TextStyle(
                fontSize: 18,
                color: Colors.grey[800],
                fontWeight: FontWeight.w600),
          ),
        ),
        body: Container(
            child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 24),
              child: Text(
                "Add Address",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 16),
              child: Column(
                children: <Widget>[
                  Container(
                    child: TextFormField(
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey[800],
                          fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                        hintText: "Address",
                        hintStyle: TextStyle(
                            fontSize: 18,
                            color: Colors.grey[800],
                            fontWeight: FontWeight.w600),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        prefixIcon: Icon(
                          Icons.mail_outline,
                          size: 22,
                        ),
                        isDense: true,
                        contentPadding: EdgeInsets.all(0),
                      ),
                      keyboardType: TextInputType.emailAddress,
                      textCapitalization: TextCapitalization.sentences,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 16),
                    child: TextFormField(
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey[800],
                          fontWeight: FontWeight.w600),
                      decoration: InputDecoration(
                        hintStyle: TextStyle(
                            fontSize: 18,
                            color: Colors.grey[800],
                            fontWeight: FontWeight.w600),
                        hintText: "Delivery note",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(8),
                            ),
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.2)),
                        prefixIcon: Icon(
                          Icons.info_outline,
                          size: 22,
                        ),
                        isDense: true,
                        contentPadding: EdgeInsets.all(0),
                      ),
                      textCapitalization: TextCapitalization.sentences,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 24),
              child: Text(
                "Payment Method",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 8),
              child: paymentMethodWidget(),
            ),
            Container(
              margin: EdgeInsets.only(left: 24, top: 16),
              child: Text(
                "Order amount :₱" + Provider.of<OrderPrice>(context).orderTotal,
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.grey[800],
                    fontWeight: FontWeight.w600),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 16),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withAlpha(20),
                        blurRadius: 3,
                        offset: Offset(0, 1),
                      ),
                    ],
                  ),
                  child: ElevatedButton(
                    onPressed: () {
                      clearOrderDetails();
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CompletedOrder()));
                    },
                    child: Text(
                      "Place Order".toUpperCase(),
                      style: TextStyle(
                          letterSpacing: 0.5,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: Color(0xFF3865f8),
                        padding:
                            EdgeInsets.symmetric(horizontal: 12, vertical: 8)),
                  ),
                ),
              ),
            ),
          ],
        )));
  }

  Widget paymentMethodWidget() {
    return Container(
      child: Column(
        children: <Widget>[
          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 0;
              });
            },
            child: Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: paymentMethod,
                  onChanged: (int? value) {
                    setState(() {
                      paymentMethod = value;
                    });
                  },
                ),
                Text(
                  "G Pay",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 1;
              });
            },
            child: Row(
              children: <Widget>[
                Radio(
                  value: 1,
                  groupValue: paymentMethod,
                  onChanged: (int? value) {
                    setState(() {
                      paymentMethod = value;
                    });
                  },
                ),
                Text(
                  "Paypal",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 2;
              });
            },
            child: Row(
              children: <Widget>[
                Radio(
                  value: 2,
                  groupValue: paymentMethod,
                  onChanged: (int? value) {
                    setState(() {
                      paymentMethod = value;
                    });
                  },
                ),
                Text(
                  "Credit card",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 3;
              });
            },
            child: Row(
              children: <Widget>[
                Radio(
                  value: 3,
                  groupValue: paymentMethod,
                  onChanged: (int? value) {
                    setState(() {
                      paymentMethod = value;
                    });
                  },
                ),
                Text(
                  "Cash on Delivery",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[800],
                      fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  clearOrderDetails() async {
    await DatabaseHelper.instance.readOrderDetailsTotal();
  }
}
