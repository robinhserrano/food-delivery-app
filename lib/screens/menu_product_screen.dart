import 'package:flutter/material.dart';

import '/models/menu_product.dart';
import '/screens/single_product_screen.dart';
import '/widgets/menu_product_item.dart';
import '/db/database.dart';

class MenuProductScreen extends StatefulWidget {
  final int categoryId;
  final String title;

  const MenuProductScreen({
    Key? key,
    required this.categoryId,
    required this.title,
  }) : super(key: key);

  @override
  _MenuProductScreenState createState() => _MenuProductScreenState();
}

class _MenuProductScreenState extends State<MenuProductScreen> {
  late List<MenuProduct> products;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshProducts();
  }

  Future refreshProducts() async {
    setState(() => isLoading = true);

    this.products = await DatabaseHelper.instance
        .readAllMenuProductByCategory(widget.categoryId);

    if (products.isEmpty) {
      this.products = await DatabaseHelper.instance
          .readAllMenuProductByCategory(widget.categoryId);
    }

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        leading: InkWell(
          onTap: () => Navigator.pop(context),
          child: Icon(
            Icons.arrow_back_ios_new,
            color: Colors.black,
          ),
        ),
        title: Text(
          widget.title,
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
      ),
      backgroundColor: Colors.white,
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : products.isEmpty
                ? Text(
                    'No Menu',
                    style: TextStyle(color: Colors.black, fontSize: 24),
                  )
                : buildCategory(),
      ),
    );
  }

  Widget buildCategory() => GridView.builder(
        padding: EdgeInsets.all(12),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1,
            crossAxisSpacing: 24,
            mainAxisSpacing: 24),
        itemCount: products.length,
        itemBuilder: (context, index) {
          final product = products[index];

          return GestureDetector(
            onTap: () async {
              await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SingleProductScreen(
                    categoryId: product.categoryId, id: product.id!),
              ));

              refreshProducts();
            },
            child: MenuProductItem(product: product, index: index),
          );
        },
      );
}
