import 'package:flutter/material.dart';
import 'package:food_delivery_application/screens/home_screen.dart';

class CompletedOrder extends StatefulWidget {
  @override
  _CompletedOrderState createState() => _CompletedOrderState();
}

class _CompletedOrderState extends State<CompletedOrder> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: buildNoItemInCart()),
    );
  }

  buildNoItemInCart() {
    return Container(
      padding: EdgeInsets.all(24),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/icon_check_cart.png",
            height: 100,
          ),
          Text(
            'Thank you for you purchase!',
            style: TextStyle(
                color: Colors.black, fontSize: 24, fontWeight: FontWeight.w700),
          ),
          SizedBox(height: 8),
          Text(
            "Order information was sent to\n your email",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.grey[700],
                fontSize: 16,
                fontWeight: FontWeight.w400),
          ),
          SizedBox(height: 8),
          ElevatedButton(
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => HomeScreen()));
            },
            child: Text(
              "Continue Shopping",
              style: TextStyle(
                  letterSpacing: 0.5,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            style: ElevatedButton.styleFrom(
                primary: Color(0xFF3865f8), // background
                padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8)),
          ),
        ],
      )),
    );
  }
}
