import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '/db/database.dart';
import '/models/order_details.dart';
import '/screens/checkout_screen.dart';
import '/screens/home_screen.dart';
import '/widgets/single_cart_item.dart';

class FoodCartScreen extends StatefulWidget {
  FoodCartScreen({
    Key? key,
    required this.isLeading,
  }) : super(key: key);

  final bool isLeading;
  @override
  _FoodCartScreenState createState() => _FoodCartScreenState();
}

class _FoodCartScreenState extends State<FoodCartScreen> {
  late List<OrderDetails> order;
  int qty = 1;
  bool isLoading = false;
  var subtotal;

  void initState() {
    getSubtotal();
    refreshOrder();
    super.initState();
  }

  Future refreshOrder() async {
    setState(() => isLoading = true);

    this.order = await DatabaseHelper.instance.readAllOrderDetails();

    if (order.isEmpty) {
      this.order = await DatabaseHelper.instance.readAllOrderDetails();
    }

    if (order.isNotEmpty) {
      subtotal = await DatabaseHelper.instance.readOrderDetailsTotal();
      updateSubtotal();
    }

    setState(() => isLoading = false);
  }

  Future getSubtotal() async {
    subtotal = await DatabaseHelper.instance.readOrderDetailsTotal();
  }

  updateSubtotal() {
    Provider.of<OrderPrice>(context, listen: false)
        .updateSubtotal(subtotal.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: widget.isLeading
            ? InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.arrow_back_ios_new,
                    size: 24, color: Colors.black),
              )
            : Container(),
        title: Text(
          "Cart",
          style: TextStyle(color: Colors.black, fontSize: 18),
        ),
      ),
      body: isLoading
          ? CircularProgressIndicator()
          : order.isEmpty
              ? buildNoItemInCart()
              : buildBody(),
    );
  }

  buildBody() {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Container(
        child: Column(
          children: <Widget>[buildCartItems(), buildPrices()],
        ),
      ),
    );
  }

  buildNoItemInCart() {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          "assets/images/icon_fries.png",
          height: 100,
        ),
        Text(
          'Hungry?',
          style: TextStyle(
              color: Colors.black, fontSize: 24, fontWeight: FontWeight.w700),
        ),
        SizedBox(height: 8),
        Text(
          "You haven't added anything to your cart!",
          style: TextStyle(
              color: Colors.grey[700],
              fontSize: 14,
              fontWeight: FontWeight.w400),
        ),
        SizedBox(height: 8),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => HomeScreen()));
          },
          child: Text(
            "Browse",
            style: TextStyle(
                letterSpacing: 0.5,
                fontWeight: FontWeight.w600,
                color: Colors.white),
          ),
          style: ElevatedButton.styleFrom(
              primary: Color(0xFF3865f8), // background
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8)),
        ),
      ],
    ));
  }

  buildCartItems() {
    return Container(
      color: Colors.grey[100],
      height: 370,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: order.length,
          itemBuilder: (context, i) {
            return SingleCartItem(
              order: order[i],
            );
          }),
    );
  }

  buildPrices() {
    return Container(
        color: Colors.white,
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24, top: 24),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Subtotal",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[700]),
                      ),
                      Text(
                        "\₱" + Provider.of<OrderPrice>(context).orderSubTotal,
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w600,
                            color: Colors.grey[700]),
                      ),
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Charges \& Taxes",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[700]),
                        ),
                        Text(
                          "\₱" + Provider.of<OrderPrice>(context).orderTax,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.grey[700]),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          "Total",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[800]),
                        ),
                        Text(
                          "\₱" + Provider.of<OrderPrice>(context).orderTotal,
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.grey[800]),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 24),
              child: Center(
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FoodCheckoutScreen()));
                  },
                  child: Text(
                    "Checkout".toUpperCase(),
                    style: TextStyle(
                        letterSpacing: 0.5,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Color(0xFF3865f8),
                      padding:
                          EdgeInsets.symmetric(horizontal: 12, vertical: 8)),
                ),
              ),
            )
          ],
        ));
  }
}

class OrderPrice extends ChangeNotifier {
  String orderSubTotal = '';
  String orderTax = '';
  String orderTotal = '';
  String counter = '';
  void updateSubtotal(String subtotal) {
    orderSubTotal = subtotal;
    orderTax = (num.parse(subtotal) * 0.12).toStringAsFixed(2);
    orderTotal =
        (num.parse(orderSubTotal) + num.parse(orderTax)).toStringAsFixed(2);
    notifyListeners();
  }

  void updateCounter(int count) {
    counter = count.toString();
    notifyListeners();
  }
}
