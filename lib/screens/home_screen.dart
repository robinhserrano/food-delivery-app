import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '/screens/food_cart_screen.dart';
import '/screens/food_menu_screen.dart';
import '/widgets/CustomBottomNavigation.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  PageController? _pageController;
  int _currentIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);

    super.initState();
  }

  @override
  void dispose() {
    _pageController!.dispose();
    super.dispose();
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return buildBody();
  }

  Scaffold buildBody() {
    return Scaffold(
      key: _scaffoldKey,
      body: PageView(
        controller: _pageController,
        onPageChanged: (index) {
          setState(() => _currentIndex = index);
        },
        children: <Widget>[
          FoodMenuScreen(),
          FoodCartScreen(isLeading: false),
        ],
        physics: NeverScrollableScrollPhysics(),
      ),
      bottomNavigationBar: CustomBottomNavigation(
        animationDuration: Duration(milliseconds: 350),
        selectedItemOverlayColor: Colors.blue,
        backgroundColor: Color(0xFF2e445c),
        selectedIndex: _currentIndex,
        onItemSelected: (index) {
          setState(() => _currentIndex = index);
          _pageController!.jumpToPage(index);
        },
        items: <CustomBottomNavigationBarItem>[
          CustomBottomNavigationBarItem(
              title: "Food Menu",
              icon: FaIcon(FontAwesomeIcons.utensils, size: 22),
              activeIcon: FaIcon(FontAwesomeIcons.utensils, size: 22),
              activeColor: Colors.white,
              inactiveColor: Colors.grey),
          CustomBottomNavigationBarItem(
              title: "Cart",
              icon: FaIcon(FontAwesomeIcons.shoppingCart, size: 22),
              activeIcon: FaIcon(FontAwesomeIcons.shoppingCart, size: 22),
              activeColor: Colors.white,
              inactiveColor: Colors.grey),
        ],
      ),
    );
  }
}
