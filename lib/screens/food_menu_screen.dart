import 'package:flutter/material.dart';
import '/models/menu_product.dart';
import '/models/menu_category.dart';
import '/screens/menu_product_screen.dart';
import '/widgets/menu_category_item.dart';
import '/db/database.dart';

class FoodMenuScreen extends StatefulWidget {
  @override
  _FoodMenuScreenState createState() => _FoodMenuScreenState();
}

class _FoodMenuScreenState extends State<FoodMenuScreen> {
  late List<MenuCategory> categories;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshCategory();
  }

  Future refreshCategory() async {
    setState(() => isLoading = true);

    this.categories = await DatabaseHelper.instance.readAllMenuCategory();

    if (categories.isEmpty) {
      print("Building data base");
      await buildDatabase();
      this.categories = await DatabaseHelper.instance.readAllMenuCategory();
    }

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: isLoading
            ? Center(child: CircularProgressIndicator())
            : categories.isEmpty
                ? Text(
                    'No Menu',
                    style: TextStyle(color: Colors.black, fontSize: 24),
                  )
                : buildBody(),
      ),
    );
  }

  buildBody() {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Container(
        padding: EdgeInsets.all(12),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[txtCategory, buildCategory()],
        ),
      ),
    );
  }

  Widget txtCategory = Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Text(
        "Category",
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
      ));

  Widget buildCategory() => GridView.builder(
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10),
        itemCount: categories.length,
        itemBuilder: (context, index) {
          final category = categories[index];

          return GestureDetector(
            onTap: () async {
              await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => MenuProductScreen(
                    categoryId: category.id!, title: category.title),
              ));
              refreshCategory();
            },
            child: MenuCategoryItem(category: category, index: index),
          );
        },
      );

  buildDatabase() async {
    await DatabaseHelper.instance.createMenuCategory(MenuCategory(
        title: "Burgers", image: "assets/images/category_burgers.png"));
    await DatabaseHelper.instance.createMenuCategory(MenuCategory(
        title: "Beverages", image: "assets/images/category_beverages.png"));
    await DatabaseHelper.instance.createMenuCategory(MenuCategory(
        title: "Combo Meals", image: "assets/images/category_combo_meals.png"));

    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Amazing Aloha Yumburger Solo",
        image: "assets/images/burger_Amazing-Aloha-Yumburger-Solo.png",
        price: 99,
        categoryId: 1));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Bacon Cheesy Yumburger Solo",
        image: "assets/images/burger_Bacon-Cheesy-Yumburger-Solo.png",
        price: 72,
        categoryId: 1));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Cheesy Deluxe Yumburger Solo",
        image: "assets/images/burger_Cheesy-Deluxe-Yumburger-Solo.png",
        price: 58,
        categoryId: 1));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Cheesy Yumburger Solo",
        image: "assets/images/burger_Cheesy-Yumburger-Solo.png",
        price: 54,
        categoryId: 1));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Yumburger Solo",
        image: "assets/images/burger_Yumburger-Solo.png",
        price: 39,
        categoryId: 1));

    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Coke",
        image: "assets/images/beverages_coke.png",
        price: 58,
        categoryId: 2));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Sprite",
        image: "assets/images/beverages_sprite.png",
        price: 58,
        categoryId: 2));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Coke McFloat",
        image: "assets/images/beverages_coke_mcfloat.png",
        price: 58,
        categoryId: 2));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "McCafe Coffee Float",
        image: "assets/images/beverages_mccafe_coffee_float.png",
        price: 59,
        categoryId: 2));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Orange Juice",
        image: "assets/images/beverages_orange_juice.png",
        price: 61,
        categoryId: 2));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "Icea Tea",
        image: "assets/images/beverages_ice_tea.png",
        price: 61,
        categoryId: 2));

    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "1 - pc. Chickenjoy",
        image: "assets/images/combo_meal_chickenjoy.png",
        price: 84,
        categoryId: 3));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "1 - pc. Chickenjoy w/ Burger Steak",
        image: "assets/images/combo_meal_chickenjoy_with_burger_steak.png",
        price: 138,
        categoryId: 3));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "1 - pc. Chickenjoy w/ Burger Steak",
        image: "assets/images/combo_meal_chickenjoy_with_fries.png",
        price: 109,
        categoryId: 3));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "1 - pc. Chickenjoy w/ Macaroni Soup",
        image: "assets/images/combo_meal_chickenjoy_with_macaroni.png",
        price: 109,
        categoryId: 3));
    await DatabaseHelper.instance.createMenuProduct(MenuProduct(
        title: "1 - pc. Chickenjoy w/ Palabok",
        image: "assets/images/combo_meal_chickenjoy_with_palabok.png",
        price: 186,
        categoryId: 3));
  }
}
