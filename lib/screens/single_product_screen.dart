import 'package:flutter/material.dart';
import 'package:food_delivery_application/models/menu_product.dart';

import 'package:food_delivery_application/widgets/single_product_item.dart';
import '/db/database.dart';

class SingleProductScreen extends StatefulWidget {
  final int categoryId;
  final int id;

  const SingleProductScreen({
    Key? key,
    required this.categoryId,
    required this.id,
  }) : super(key: key);

  @override
  _SingleProductScreenState createState() => _SingleProductScreenState();
}

class _SingleProductScreenState extends State<SingleProductScreen> {
  late List<MenuProduct> products;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();

    refreshSingleProduct();
  }

  Future refreshSingleProduct() async {
    setState(() => isLoading = true);

    this.products = await DatabaseHelper.instance
        .readMenuProduct(widget.categoryId, widget.id);

    if (products.isEmpty) {
      this.products = await DatabaseHelper.instance
          .readMenuProduct(widget.categoryId, widget.id);
    }

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : products.isEmpty
                ? Text(
                    'No Menu',
                    style: TextStyle(color: Colors.black, fontSize: 24),
                  )
                : buildCategory(),
      ),
    );
  }

  Widget buildCategory() => SingleProductItem(product: products[0]);
}
