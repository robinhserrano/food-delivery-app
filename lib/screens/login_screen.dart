import 'package:flutter/material.dart';
import '/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Center(
        child: Container(
            padding: EdgeInsets.all(24),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Image.asset(
                        './assets/images/food_app_logo.png',
                        width: 120,
                        height: 120,
                      ),
                    ),
                    Container(
                      child: Text(
                        "Food Delivery".toUpperCase(),
                        style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey[800],
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.5),
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      color: Color(0xfff3f4f8),
                      alignment: Alignment.center,
                      child: Container(
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: 'Email',
                              contentPadding: EdgeInsets.all(15),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              )),
                        ),
                      ),
                    ),
                    SizedBox(height: 12),
                    Container(
                      color: Color(0xfff3f4f8),
                      alignment: Alignment.center,
                      child: Container(
                        child: TextFormField(
                          decoration: InputDecoration(
                              hintText: 'Password',
                              contentPadding: EdgeInsets.all(15),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                              )),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      width: double.infinity,
                      child: ElevatedButton(
                          onPressed: () {
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => HomeScreen()));
                          },
                          child: Text("LOGIN")),
                    )
                  ],
                ),
              ),
            )),
      )),
    );
  }
}
