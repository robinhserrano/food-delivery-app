import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '/models/menu_product.dart';
import '/models/order_details.dart';
import '/models/menu_category.dart';

class DatabaseHelper {
  static final DatabaseHelper instance = DatabaseHelper._init();

  static Database? _database;

  DatabaseHelper._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('food_menu.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 2, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    final textType = 'TEXT NOT NULL';
    final integerType = 'INTEGER NOT NULL';
    final realType = 'REAL NOT NULL';

    await db.execute('''
CREATE TABLE $tableMenuCategory ( 
  ${MenuCategoryFields.id} $idType, 
  ${MenuCategoryFields.title} $textType,
  ${MenuCategoryFields.image} $textType
  )
''');

    await db.execute('''
CREATE TABLE $tableMenuProduct ( 
  ${MenuProductFields.id} $idType, 
  ${MenuProductFields.title} $textType,
  ${MenuProductFields.image} $textType,
  ${MenuProductFields.price} $realType,
  ${MenuProductFields.categoryId} $integerType
  )
''');

    await db.execute('''
CREATE TABLE $tableOrderDetails ( 
  ${OrderDetailsFields.id} $idType, 
  ${OrderDetailsFields.menuProductId} $integerType,
  ${OrderDetailsFields.amount} $realType,
  ${OrderDetailsFields.noOfServing} $integerType,
  ${OrderDetailsFields.totalAmount} $realType
  )
''');
  }

  Future<MenuCategory> createMenuCategory(MenuCategory category) async {
    final db = await instance.database;

    final id = await db.insert(tableMenuCategory, category.toJson());
    return category.copy(id: id);
  }

  Future<List<MenuCategory>> readAllMenuCategory() async {
    final db = await instance.database;

    final orderBy = '${MenuCategoryFields.title} ASC';
    final result = await db.query(tableMenuCategory, orderBy: orderBy);

    return result.map((json) => MenuCategory.fromJson(json)).toList();
  }

  Future<MenuProduct> createMenuProduct(MenuProduct product) async {
    final db = await instance.database;

    final id = await db.insert(tableMenuProduct, product.toJson());
    return product.copy(id: id);
  }

  Future<List<MenuProduct>> readAllMenuProduct() async {
    final db = await instance.database;

    final orderBy = '${MenuProductFields.title} ASC';
    final result = await db.query(tableMenuProduct, orderBy: orderBy);

    return result.map((json) => MenuProduct.fromJson(json)).toList();
  }

  Future<List<MenuProduct>> readAllMenuProductByCategory(int categoryId) async {
    final db = await instance.database;

    final orderBy = '${MenuProductFields.title} ASC';
    final result = await db.query(tableMenuProduct,
        columns: MenuProductFields.values,
        where: '${MenuProductFields.categoryId} = ?',
        whereArgs: [categoryId],
        orderBy: orderBy);

    return result.map((json) => MenuProduct.fromJson(json)).toList();
  }

  Future<List<MenuProduct>> readMenuProduct(int categoryId, int id) async {
    final db = await instance.database;
    final orderBy = '${MenuProductFields.title} ASC';

    final result = await db.query(tableMenuProduct,
        columns: MenuProductFields.values,
        where:
            '${MenuProductFields.categoryId} = ? and ${MenuProductFields.id} = ?',
        whereArgs: [categoryId, id],
        orderBy: orderBy);

    return result.map((json) => MenuProduct.fromJson(json)).toList();
  }

  Future<List<MenuProduct>> readMenuProductById(int menuProductId) async {
    final db = await instance.database;
    final orderBy = '${MenuProductFields.title} ASC';

    final result = await db.query(tableMenuProduct,
        columns: MenuProductFields.values,
        where: '${MenuProductFields.id} = ?',
        whereArgs: [menuProductId],
        orderBy: orderBy);

    return result.map((json) => MenuProduct.fromJson(json)).toList();
  }

  Future<OrderDetails> addOrderDetails(OrderDetails order) async {
    final db = await instance.database;

    final id = await db.insert(tableOrderDetails, order.toJson());
    return order.copy(id: id);
  }

  Future<int> updateOrderDetails(OrderDetails order) async {
    final db = await instance.database;

    return db.update(
      tableOrderDetails,
      order.toJson(),
      where: '${OrderDetailsFields.id} = ?',
      whereArgs: [order.id],
    );
  }

  Future<List<OrderDetails>> readAllOrderDetails() async {
    final db = await instance.database;

    final result = await db.query(tableOrderDetails);

    return result.map((json) => OrderDetails.fromJson(json)).toList();
  }

  Future<List<OrderDetails>> readOrderDetails(int menuProductId) async {
    final db = await instance.database;

    final result = await db.query(
      tableOrderDetails,
      columns: OrderDetailsFields.values,
      where: '${OrderDetailsFields.menuProductId} = ?',
      whereArgs: [menuProductId],
    );

    return result.map((json) => OrderDetails.fromJson(json)).toList();
  }

  Future<int> removeOrderDetails(int orderDetailsId) async {
    final db = await instance.database;

    return await db.delete(
      tableOrderDetails,
      where: '${OrderDetailsFields.id} = ?',
      whereArgs: [orderDetailsId],
    );
  }

  Future<void> removeAllOrderDetails(int orderDetailsId) async {
    final db = await instance.database;
    await db.execute("delete from $tableOrderDetails");
  }

  Future<num> readOrderDetailsTotal() async {
    final db = await instance.database;

    List<Map> result = await db.rawQuery(
      'SELECT SUM(total_amount) FROM OrderDetails',
    );

    String total = result[0].values.toString().split('(')[1].split(')')[0];
    return num.parse(total);
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }
}
