final String tableMenuProduct = "menuProduct";

class MenuProductFields {
  static final List<String> values = [
    /// Add all fields
    id, title, image, price, categoryId
  ];

  static final String id = 'menuproduct_id';
  static final String title = 'title';
  static final String image = 'image';
  static final String price = 'price';
  static final String categoryId = 'category_id';
}

class MenuProduct {
  final int? id;

  final String title;
  final String image;
  final num price;
  final int categoryId;

  const MenuProduct({
    this.id,
    required this.title,
    required this.image,
    required this.price,
    required this.categoryId,
  });

  MenuProduct copy(
          {int? id,
          String? title,
          String? image,
          num? price,
          int? categoryId}) =>
      MenuProduct(
        id: id ?? this.id,
        title: title ?? this.title,
        image: image ?? this.image,
        price: price ?? this.price,
        categoryId: categoryId ?? this.categoryId,
      );

  static MenuProduct fromJson(Map<String, Object?> json) => MenuProduct(
        id: json[MenuProductFields.id] as int?,
        title: json[MenuProductFields.title] as String,
        image: json[MenuProductFields.image] as String,
        price: json[MenuProductFields.price] as num,
        categoryId: json[MenuProductFields.categoryId] as int,
      );

  Map<String, Object?> toJson() => {
        MenuProductFields.id: id,
        MenuProductFields.title: title,
        MenuProductFields.image: image,
        MenuProductFields.price: price,
        MenuProductFields.categoryId: categoryId,
      };
}
