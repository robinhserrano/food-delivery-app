final String tableOrderDetails = "OrderDetails";

class OrderDetailsFields {
  static final List<String> values = [
    /// Add all fields
    id, menuProductId, amount, noOfServing, totalAmount
  ];

  static final String id = 'order_details_id';
  static final String menuProductId = 'menuproduct_id';
  static final String amount = 'amount';
  static final String noOfServing = 'no_of_serving';
  static final String totalAmount = 'total_amount';
}

class OrderDetails {
  final int? id;

  final int menuProductId;
  final num amount;
  final int noOfServing;
  final num totalAmount;

  const OrderDetails({
    this.id,
    required this.menuProductId,
    required this.amount,
    required this.noOfServing,
    required this.totalAmount,
  });

  OrderDetails copy({
    int? id,
    int? menuProductId,
    num? amount,
    int? noOfServing,
    num? totalAmount,
  }) =>
      OrderDetails(
        id: id ?? this.id,
        menuProductId: menuProductId ?? this.menuProductId,
        amount: amount ?? this.amount,
        noOfServing: noOfServing ?? this.noOfServing,
        totalAmount: totalAmount ?? this.totalAmount,
      );

  static OrderDetails fromJson(Map<String, Object?> json) => OrderDetails(
        id: json[OrderDetailsFields.id] as int?,
        menuProductId: json[OrderDetailsFields.menuProductId] as int,
        amount: json[OrderDetailsFields.amount] as num,
        noOfServing: json[OrderDetailsFields.noOfServing] as int,
        totalAmount: json[OrderDetailsFields.totalAmount] as num,
      );

  Map<String, Object?> toJson() => {
        OrderDetailsFields.id: id,
        OrderDetailsFields.menuProductId: menuProductId,
        OrderDetailsFields.amount: amount,
        OrderDetailsFields.noOfServing: noOfServing,
        OrderDetailsFields.totalAmount: totalAmount,
      };
}
