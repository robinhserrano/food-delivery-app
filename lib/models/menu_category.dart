final String tableMenuCategory = "menuCategory";

class MenuCategoryFields {
  static final List<String> values = [
    /// Add all fields
    id, title, image
  ];

  static final String id = 'category_id';
  static final String title = 'title';
  static final String image = 'image';
}

class MenuCategory {
  final int? id;

  final String title;
  final String image;

  const MenuCategory({
    this.id,
    required this.title,
    required this.image,
  });

  MenuCategory copy({
    int? id,
    String? title,
    String? image,
  }) =>
      MenuCategory(
        id: id ?? this.id,
        title: title ?? this.title,
        image: image ?? this.image,
      );

  static MenuCategory fromJson(Map<String, Object?> json) => MenuCategory(
        id: json[MenuCategoryFields.id] as int?,
        title: json[MenuCategoryFields.title] as String,
        image: json[MenuCategoryFields.image] as String,
      );

  Map<String, Object?> toJson() => {
        MenuCategoryFields.id: id,
        MenuCategoryFields.title: title,
        MenuCategoryFields.image: image,
      };
}
